package com.warathip.week10;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Rectangle rec = new Rectangle(5, 3);
        System.out.println(rec.toString());
        System.out.println(rec.calArea());
        System.out.println(rec.calPeritmeter());

        Rectangle rec2 = new Rectangle(2, 2);
        System.out.println(rec2.toString());
        System.out.println(rec2.calArea());
        System.out.println(rec2.calPeritmeter());

        Circle circle1 = new Circle(2);
        System.out.println(circle1);
        System.out.printf("%s are: %.3f \n",circle1.getName(),circle1.calArea());
        System.out.printf("%s perimeter: %.3f \n",circle1.getName(),circle1.calPeritmeter());

        Circle circle2 = new Circle(3);
        System.out.println(circle2);
        System.out.printf("%s are: %.3f \n",circle1.getName(),circle2.calArea());
        System.out.printf("%s perimeter: %.3f \n",circle1.getName(),circle2.calPeritmeter());

        Circle circle3 = new Circle(5);
        System.out.println(circle3);
        System.out.printf("%s are: %.3f \n",circle1.getName(),circle3.calArea());
        System.out.printf("%s perimeter: %.3f \n",circle1.getName(),circle3.calPeritmeter());

        Triangle triangle1 = new Triangle(2, 2, 2);
        System.out.print(triangle1);
        System.out.printf("%s are: %.3f \n ",triangle1.getName(),triangle1.calArea());
        System.out.printf("%s perimeter: %.3f \n",triangle1.getName(),triangle1.calPeritmeter());
    }
}
